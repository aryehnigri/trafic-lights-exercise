import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PedestrianMooving extends Thread {
	JLabel myLabel;
	JPanel myPanel;
	private PedestrianLight myRamzor;
	private int key;
	int x, dx;
	int y, dy;
	ImageIcon imageIcon;
	boolean first2 = true;

	public PedestrianMooving(JPanel myPanel, PedestrianLight myRamzor, int key) {
		this.myPanel = myPanel;
		this.myRamzor = myRamzor;
		this.key = key;
		setCarLocationAndMooving();
		imageIcon = getImageIcon();
		myLabel = new JLabel(imageIcon);
		myLabel.setOpaque(false);
		myPanel.add(myLabel);
		// System.out.println("Car Drawn");
		setDaemon(true);
		start();
	}

	private void setCarLocationAndMooving() {
		switch (key) {
		case 1:
			x = 850;
			dx = -100;
			y = 70;
			dy = 0;
			break;
		case 2:
			x = 500;
			dx = 0;
			y = 720;
			dy = -50;
			break;
		case 3:
			x = -30;
			dx = 35;
			y = 405;
			dy = 22;
			break;
		case 4:
			x = -30;
			dx = 50;
			y = 390;
			dy = 0;
			break;
		case 5:
			dx = -50;
			dy = 0;
			break;
		case 6:
			x = 850;
			dx = -100;
			y = 130;
			dy = 0;
			break;
		case 7:
			dx = 0;
			dy = 50;
			break;
		default:
			x = 900;
			dx = -50;
			y = 100;
			dy = 0;
			break;
		}
	}

	public void run() {
		myLabel.setBounds(x, y, imageIcon.getIconWidth(), imageIcon.getIconHeight());

		while (!finish()) {
			if (myRamzor.isStop() && toStop())
				;
			else {
				x += dx;
				y += dy;
				myLabel.setBounds(x, y, imageIcon.getIconWidth(), imageIcon.getIconHeight());
			}
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			myPanel.repaint();
		}

	}

	private boolean finish() {
		switch (key) {
		case 1:
			return x < -20;
		case 2:
			if (y > 180)
				return false;
			else {
				key = 5;
				setCarLocationAndMooving();
				imageIcon = getImageIcon();
				myLabel.removeAll();
				myLabel.setIcon(imageIcon);
				myLabel.setBounds(x, y, imageIcon.getIconWidth(), imageIcon.getIconHeight());
				return false;
			}
		case 3:
			if (y > 600) {
				dx = 0;
				dy = 30;
			}
			return y > 800;
		case 4:
			return x > 800;
		case 5:
			return x < -20;
		case 6:
			if (x < 400) {
				key = 7;
				setCarLocationAndMooving();
				imageIcon = getImageIcon();
				myLabel.removeAll();
				myLabel.setIcon(imageIcon);
				myLabel.setBounds(x, y, imageIcon.getIconWidth(), imageIcon.getIconHeight());
				return false;
			}
			return false;
		case 7:
			return y > 800;
		}
		return false;
	}

	private boolean toStop() {
		switch (key) {
		case 6:
		case 1:
			return x > 550;
		case 2:
			return y > 530;
		case 3:
			return x < 100;
		case 4:
			return x <= 150;
		// case 6:
		// return x > 550;
		}
		return false;
	}

	private ImageIcon getImageIcon() {
		switch (key) {
		case 1:
            // return new ImageIcon("../Images/left.gif");
            return new ImageIcon("../Images/walker32px.png");
		case 2:
			// return new ImageIcon("../Images/up.gif");
            return new ImageIcon("../Images/walker32px.png");
		case 3:
		case 4:
			// return new ImageIcon("../Images/right.gif");
            return new ImageIcon("../Images/walker32px.png");
		case 5:
		case 6:
			// return new ImageIcon("../Images/left.gif");
            return new ImageIcon("../Images/walker32px.png");
		case 7:
			// return new ImageIcon("../Images/down.gif");
            return new ImageIcon("../Images/walker32px.png");
		default:
			break;
		}
		return null;
	}

}
