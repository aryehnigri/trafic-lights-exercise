import java.awt.Color;
import javax.swing.JPanel;

public class FlashingLight extends Thread {
    Ramzor ramzor;
    JPanel panel;

    enum State {
        ON, OFF;
    };

    State state;

    public FlashingLight(Ramzor ramzor, JPanel panel) {
        this.ramzor = ramzor;
        this.panel = panel;

        setDaemon(true);
        start();
    }

    Timer78 timer;
    Event evTimer = new Event();

    @Override
    public void run() {
        state = State.ON;
        turnYellow();

        while (true) {
            switch (state) {
                case ON:
                    while (true) {
                        if (evTimer.arrivedEvent()) {
                            evTimer.waitEvent();
                            turnGray();
                            state = State.OFF;
                            break;
                        } else
                            yield();
                    }
                    break;

                case OFF:
                    while (true) {
                        if (evTimer.arrivedEvent()) {
                            evTimer.waitEvent();
                            turnYellow();
                            state = State.ON;
                            break;
                        } else
                            yield();
                    }
                    break;
            }
        }

    }

    public void setLight(int place, Color color) {
        ramzor.colorLight[place - 1] = color;
        panel.repaint();
    }

    final long flashingTime = 1000;

    private void turnGray() {
        setLight(1, Color.LIGHT_GRAY);
        timer = new Timer78(flashingTime, evTimer);
    }

    private void turnYellow() {
        setLight(1, Color.YELLOW);
        timer = new Timer78(flashingTime, evTimer);
    }

}
