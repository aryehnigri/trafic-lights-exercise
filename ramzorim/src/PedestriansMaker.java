import javax.swing.JPanel;

public class PedestriansMaker extends Thread {
	JPanel myPanel;
	private PedestrianLight myRamzor;
	int key;

	public PedestriansMaker(JPanel myPanel, PedestrianLight myRamzor, int key) {
		this.myPanel = myPanel;
		this.myRamzor = myRamzor;
		this.key = key;
		setDaemon(true);
		start();
	}

	public void run() {
		try {
			while (true) {
				sleep(300);
				if (!myRamzor.isStop()) {
					new PedestrianMooving(myPanel, myRamzor, key);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
