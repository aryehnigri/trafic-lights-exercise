import java.awt.Color;
import javax.swing.JPanel;

public class PedestrianLight extends Thread {
    private Event evRed, evGreen, evShabbat, evAck;

    enum OutState {
        WEEKDAY, SHABBAT;
    };

    OutState outState;

    // enum InState {
    // GREEN, RED;
    // };
    enum InState {
        GREEN, RED, RED_TO_GREEN, GREEN_OFF, GREEN_ON;
    };

    InState inState;
    Ramzor ramzor;
    JPanel panel;
    private boolean stop = true;

    public PedestrianLight(Ramzor ramzor, JPanel panel, Event evRed, Event evGreen, Event evShabbat, Event evAck,
            int key) {
        this.ramzor = ramzor;
        this.panel = panel;

        this.evRed = evRed;
        this.evGreen = evGreen;
        this.evShabbat = evShabbat;
        this.evAck = evAck;

        // new PedestriansMaker(panel, this, key);

        setDaemon(true);
        start();
    }

    public boolean isStop() {
        return stop;
    }

    Timer78 timer;
    Event evTimer = new Event();
    final long greenFlashingTime = 500;

    @Override
    public void run() {

        // int greeenTime = 4000;
        int counter = 0;
        boolean out = false;
        outState = OutState.WEEKDAY;
        inState = InState.RED;
        turnRedAndSendAck();

        while (true) {
            switch (outState) {
            case WEEKDAY:
                while (!out) {
                    switch (inState) {
                    case GREEN:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                turnGray();
                                out = true;
                                outState = OutState.SHABBAT;
                                break;
                            } else if (evRed.arrivedEvent()) {
                                // System.out.println("PedestrianLight evRed");
                                evRed.waitEvent();
                                // turnRedAndSendAck();
                                // inState = InState.RED;
                                setLight(2, Color.GRAY);
                                counter = 0;
                                timer = new Timer78(greenFlashingTime, evTimer);
                                inState = InState.GREEN_OFF;
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                evGreen.waitEvent();
                                // System.out.println("PedestrianLight evGreen NOT EXPECTED HERE");
                                break;
                            } else
                                yield();
                        }
                        break;

                        case GREEN_OFF:
                            while (true) {
                                if (evShabbat.arrivedEvent()) {
                                    evShabbat.waitEvent();
                                    turnGray();
                                    out = true;
                                    timer.cancel();
                                    outState = OutState.SHABBAT;
                                    break;
                                } else if (evRed.arrivedEvent()) {
                                    evRed.waitEvent();
                                    // System.out.println("PedestrianLight evRed NOT EXPECTED HERE");
                                    break;
                                } else if (evGreen.arrivedEvent()) {
                                    evGreen.waitEvent();
                                    // System.out.println("PedestrianLight evGreen NOT EXPECTED HERE");
                                    break;
                                } else if(evTimer.arrivedEvent()){
                                    evTimer.waitEvent();
                                    setLight(2, Color.GREEN);
                                    timer = new Timer78(greenFlashingTime, evTimer);
                                    counter++;
                                    inState = InState.GREEN_ON;
                                    break;
                                } 
                                else
                                    yield();
                            }
                            break;

                        case GREEN_ON:
                            while (true) {
                                if (evShabbat.arrivedEvent()) {
                                    evShabbat.waitEvent();
                                    turnGray();
                                    out = true;
                                    timer.cancel();
                                    outState = OutState.SHABBAT;
                                    break;
                                } else if (evRed.arrivedEvent()) {
                                    evRed.waitEvent();
                                    // System.out.println("PedestrianLight evRed NOT EXPECTED HERE");
                                    break;
                                } else if (evGreen.arrivedEvent()) {
                                    evGreen.waitEvent();
                                    // System.out.println("PedestrianLight evGreen NOT EXPECTED HERE");
                                    break;
                                }else if(counter >= 3){
                                    turnRedAndSendAck();
                                    inState = InState.RED;
                                    break;
                                } else if(evTimer.arrivedEvent()){
                                    evTimer.waitEvent();
                                    setLight(2, Color.GRAY);
                                    timer = new Timer78(greenFlashingTime, evTimer);
                                    inState = InState.GREEN_OFF;
                                    break;
                                }
                                else
                                    yield();
                            }
                            break;

                    case RED:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                turnGray();
                                out = true;
                                outState = OutState.SHABBAT;
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                // System.out.println("PedestrianLight evGreen");
                                evGreen.waitEvent();
                                timer = new Timer78(1000, evTimer);
                                // try {
                                // sleep(1000);
                                // } catch (InterruptedException exce) {
                                // }
                                // turnGreen();
                                // inState = InState.GREEN;
                                inState = InState.RED_TO_GREEN;
                                break;
                            } else if (evRed.arrivedEvent()) {
                                evRed.waitEvent();
                                // System.out.println("PedestrianLight evRed NOT EXPECTED HERE");
                                break;
                            } else
                                yield();
                        }
                        break;

                    // NEW STATE
                    case RED_TO_GREEN:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                turnGray();
                                out = true;
                                timer.cancel();
                                outState = OutState.SHABBAT;
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                evGreen.waitEvent();
                                // System.out.println("PedestrianLight evGreen NOT EXPECTED HERE");
                                break;
                            } else if (evRed.arrivedEvent()) {
                                evRed.waitEvent();
                                // System.out.println("PedestrianLight evRed NOT EXPECTED HERE");
                                break;
                            } else if (evTimer.arrivedEvent()) {
                                evTimer.waitEvent();
                                turnGreen();
                                inState = InState.GREEN;
                                break;
                            } else
                                yield();
                        }
                        break;
                    }
                }
                break;

            case SHABBAT: {
                evShabbat.waitEvent();
                out = false;
                turnRedAndSendAck();
                outState = OutState.WEEKDAY;
                inState = InState.RED;
                break;
            }
            }
        }

    }

    public void setLight(int place, Color color) {
        ramzor.colorLight[place - 1] = color;
        panel.repaint();
    }

    private void turnGreen() {
        // System.out.println("PedestrianLight GREEN");
        setLight(1, Color.GRAY);
        setLight(2, Color.GREEN);
        stop = false;
    }

    private void turnRedAndSendAck() {
        stop = true;
        setLight(1, Color.RED);
        setLight(2, Color.GRAY);
        evAck.sendEvent();
    }

    private void turnGray() {
        // System.out.println("PedestrianLight GRAY");
        setLight(1, Color.GRAY);
        setLight(2, Color.GRAY);
    }

}
