public class Timer78 extends Thread {
    private final long time;
    private final Event evTimer;
    private boolean cancel = false;

    public Timer78(long time, Event evTimer) {
        this.time = time;
        this.evTimer = evTimer;
        setDaemon(true);
        start();
    }

    public void cancel() {
        cancel = true;
    }

    public void run() {
        try {
            sleep(time);
        } catch (InterruptedException ex) {
        }
        if (!cancel)
            evTimer.sendEvent();
    }

}