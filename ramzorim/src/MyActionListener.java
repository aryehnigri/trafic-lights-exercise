import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.AbstractButton;
import javax.swing.JRadioButton;

public class MyActionListener implements ActionListener {

	private Event evButton, evShabbat;
	// private Controller controller;

	public MyActionListener(Event evButton, Event evShabbat) {// , Controller controller) {
		this.evButton = evButton;
		this.evShabbat = evShabbat;
		// this.controller = controller;
	}

	public void actionPerformed(ActionEvent e) {

		JRadioButton butt = (JRadioButton) e.getSource();
		// AbstractButton butt = (AbstractButton) e.getSource();
		int buttonNumber = Integer.parseInt(butt.getName());

		// System.out.println("Button pressed: " + buttonNumber);

		// if (buttonNumber == 16) {
		// 	evShabbat.sendEvent();
		// } else {
		// 	// butt.setSelected(false);
		// 	button.sendEvent(buttonNumber);
		// }
		if(buttonNumber != 16){
			butt.setSelected(false);
		}

		new Thread() {
			public void run() {
				if(buttonNumber == 16){
					evShabbat.sendEvent();
				}else{
					evButton.sendEvent(buttonNumber);
				}
			}
		}.start();
	}

}
