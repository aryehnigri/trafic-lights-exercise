import java.awt.Color;
import javax.swing.JPanel;

public class CarLight extends Thread {
    private Event evRed, evGreen, evShabbat, evAck;
    private int counter;

    enum OutState {
        WEEKDAY, SHABBAT;
    };

    OutState outState;

    enum InWeekdayState {
        GREEN, GREEN_OFF, GREEN_ON, YELLOW_TO_RED, YELLOW_TO_GREEN, RED;
    };

    InWeekdayState inWeekdayState;

    enum InShabbatState {
        ON, OFF;
    }

    InShabbatState inShabbatState;
    Ramzor ramzor;
    JPanel panel;
    private boolean stop = true;

    public CarLight(Ramzor ramzor, JPanel panel, Event evRed, Event evGreen, Event evShabbat, Event evAck, int key) {
        this.ramzor = ramzor;
        this.panel = panel;

        this.evRed = evRed;
        this.evGreen = evGreen;
        this.evShabbat = evShabbat;
        this.evAck = evAck;

        new CarsMaker(panel, this, key);
        if(key == 1){
        new CarsMaker(panel, this, 6);//going down
        }

        setDaemon(true);
        start();
    }

    public boolean isStop() {
        return stop;
    }

    Timer78 timer;
    Event evTimer = new Event();

    @Override
    public void run() {

        boolean outWeek = false, outShabbat = false;
        outState = OutState.WEEKDAY;
        inWeekdayState = InWeekdayState.RED;
        inShabbatState = InShabbatState.ON;
        turnRedAndSendAck();

        while (true) {
            switch (outState) {
            case WEEKDAY:
                while (!outWeek) {
                    switch (inWeekdayState) {
                    case GREEN:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                outShabbat = false;
                                turnYellow();
                                outWeek = true;
                                outState = OutState.SHABBAT;
                                inShabbatState = InShabbatState.ON;
                                break;
                            } else if (evRed.arrivedEvent()) {
                                evRed.waitEvent();
                                // turnGray();
                                setLight(3, Color.LIGHT_GRAY);
                                counter = 0;
                                timer = new Timer78(greenFlashingTime, evTimer);
                                inWeekdayState = InWeekdayState.GREEN_OFF;
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                evGreen.waitEvent();
                                // System.out.println("CarLight evGreen NOT EXPECTED HERE");
                                break;
                            } else
                                yield();
                        }
                        break;

                    case GREEN_OFF:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                outShabbat = false;
                                turnYellow();
                                outWeek = true;
                                timer.cancel();
                                outState = OutState.SHABBAT;
                                inShabbatState = InShabbatState.ON;
                                break;
                            } else if (evRed.arrivedEvent()) {
                                evRed.waitEvent();
                                // System.out.println("CarLight evRed NOT EXPECTED HERE");
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                evGreen.waitEvent();
                                // System.out.println("CarLight evGreen NOT EXPECTED HERE");
                                break;
                            } else if (evTimer.arrivedEvent()) {
                                evTimer.waitEvent();
                                setLight(3, Color.GREEN);
                                timer = new Timer78(greenFlashingTime, evTimer);
                                counter++;
                                inWeekdayState = InWeekdayState.GREEN_ON;
                                break;
                            } else
                                yield();

                        }
                        break;

                    case GREEN_ON:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                outShabbat = false;
                                turnYellow();
                                outWeek = true;
                                timer.cancel();
                                outState = OutState.SHABBAT;
                                inShabbatState = InShabbatState.ON;
                                break;
                            } else if (evRed.arrivedEvent()) {
                                evRed.waitEvent();
                                // System.out.println("CarLight evRed NOT EXPECTED HERE");
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                evGreen.waitEvent();
                                // System.out.println("CarLight evGreen NOT EXPECTED HERE");
                                break;
                            } else if (counter >= 3) {
                                turnYellow();
                                timer = new Timer78(yellowTime, evTimer);
                                inWeekdayState = InWeekdayState.YELLOW_TO_RED;
                                break;
                            } else if (evTimer.arrivedEvent()) {
                                evTimer.waitEvent();
                                // turnGray();
                                setLight(3, Color.LIGHT_GRAY);
                                timer = new Timer78(greenFlashingTime, evTimer);
                                inWeekdayState = InWeekdayState.GREEN_OFF;
                                break;
                            } else
                                yield();
                        }
                        break;

                    case YELLOW_TO_RED:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                outShabbat = false;
                                turnYellow();
                                outWeek = true;
                                timer.cancel();
                                outState = OutState.SHABBAT;
                                inShabbatState = InShabbatState.ON;
                                break;
                            } else if (evRed.arrivedEvent()) {
                                evRed.waitEvent();
                                // System.out.println("CarLight evRed NOT EXPECTED HERE");
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                evGreen.waitEvent();
                                // System.out.println("CarLight evGreen NOT EXPECTED HERE");
                                break;
                            } else if (evTimer.arrivedEvent()) {
                                evTimer.waitEvent();
                                turnRedAndSendAck();
                                inWeekdayState = InWeekdayState.RED;
                                break;
                            } else
                                yield();
                        }
                        break;

                    case YELLOW_TO_GREEN:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                outShabbat = false;
                                turnYellow();
                                outWeek = true;
                                timer.cancel();
                                outState = OutState.SHABBAT;
                                inShabbatState = InShabbatState.ON;
                                break;
                            } else if (evTimer.arrivedEvent()) {
                                evTimer.waitEvent();
                                turnGreen();
                                inWeekdayState = InWeekdayState.GREEN;
                                break;
                            } else if (evRed.arrivedEvent()) {
                                evRed.waitEvent();
                                // System.out.println("CarLight evRed NOT EXPECTED HERE");
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                evGreen.waitEvent();
                                // System.out.println("CarLight evGreen NOT EXPECTED HERE");
                                break;
                            } else
                                yield();
                        }
                        break;

                    case RED:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                outShabbat = false;
                                turnYellow();
                                outWeek = true;
                                outState = OutState.SHABBAT;
                                inShabbatState = InShabbatState.ON;
                                break;
                            } else if (evGreen.arrivedEvent()) {
                                evGreen.waitEvent();
                                setLight(2, Color.YELLOW);
                                timer = new Timer78(yellowTime, evTimer);
                                inWeekdayState = InWeekdayState.YELLOW_TO_GREEN;
                                break;
                            } else if (evRed.arrivedEvent()) {
                                evRed.waitEvent();
                                // System.out.println("CarLight evRed NOT EXPECTED HERE");
                                break;
                            } else
                                yield();
                        }
                        break;
                    }
                }
                break;

            case SHABBAT:
                while (!outShabbat) {
                    switch (inShabbatState) {
                    case ON:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                outWeek = false;
                                turnRedAndSendAck();
                                outShabbat = true;
                                timer.cancel();
                                outState = OutState.WEEKDAY;
                                inWeekdayState = InWeekdayState.RED;
                                break;
                            } else if (evTimer.arrivedEvent()) {
                                evTimer.waitEvent();
                                turnGray();
                                timer = new Timer78(flashingTime, evTimer);
                                inShabbatState = InShabbatState.OFF;
                                break;
                            } else
                                yield();
                        }
                        break;

                    case OFF:
                        while (true) {
                            if (evShabbat.arrivedEvent()) {
                                evShabbat.waitEvent();
                                outWeek = false;
                                turnRedAndSendAck();
                                outShabbat = true;
                                timer.cancel();
                                outState = OutState.WEEKDAY;
                                inWeekdayState = InWeekdayState.RED;
                                break;
                            } else if (evTimer.arrivedEvent()) {
                                evTimer.waitEvent();
                                turnYellow();
                                timer = new Timer78(flashingTime, evTimer);
                                inShabbatState = InShabbatState.ON;
                                break;
                            } else
                                yield();
                        }
                        break;
                    }
                }
                break;
            }
        }

    }

    public void setLight(int place, Color color) {
        ramzor.colorLight[place - 1] = color;
        panel.repaint();
    }

    final long greenFlashingTime = 500;
    final long flashingTime = 1000;
    final long yellowTime = 1000;

    private void turnYellow() {
        setLight(1, Color.LIGHT_GRAY);
        setLight(2, Color.YELLOW);
        setLight(3, Color.LIGHT_GRAY);
        stop = true;
    }

    private void turnGreen() {
        stop = false;
        setLight(1, Color.LIGHT_GRAY);
        setLight(2, Color.LIGHT_GRAY);
        setLight(3, Color.GREEN);
    }

    private void turnGray() {
        setLight(1, Color.LIGHT_GRAY);
        setLight(2, Color.LIGHT_GRAY);
        setLight(3, Color.LIGHT_GRAY);
    }

    private void turnRedAndSendAck() {
        setLight(1, Color.RED);
        setLight(2, Color.LIGHT_GRAY);
        setLight(3, Color.LIGHT_GRAY);
        evAck.sendEvent();
    }

}