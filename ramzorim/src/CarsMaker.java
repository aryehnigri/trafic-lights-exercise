import java.util.ArrayList;

import javax.swing.JPanel;

public class CarsMaker extends Thread {
	JPanel myPanel;
	private CarLight myRamzor;
	int key;
	CarMooving carMoovings[];
	int numberOfCars;
	int index;

	public CarsMaker(JPanel myPanel, CarLight myRamzor, int key) {
		this.myPanel = myPanel;
		this.myRamzor = myRamzor;
		this.key = key;

		numberOfCars = key;
		carMoovings = new CarMooving[numberOfCars];
		index = 0;

		setDaemon(true);
		start();
	}

	public void run() {
		try {
			while (true) {
				sleep(500);
				if (!myRamzor.isStop()) {
					if(index == numberOfCars){
						index = 0;
					}
					carMoovings[index] = new CarMooving(myPanel, myRamzor, key);
					index++;
					// new CarMooving(myPanel, myRamzor, key);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
